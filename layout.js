importScripts("https://d3js.org/d3.v4.min.js");


layout = (function(){
	var pos = {};
	var newNodes = null;
	var newLinks = null;


	/**
	 * Pls use this function.
	 * If you want to test it, pls uncomment out the lines has postMessage function;
	 * @param  {[type]} nodes   [description]
	 * @param  {[type]} bb      [description]
	 * @param  {[type]} options [description]
	 * @return {Object}         data structure: {
	 *                               				id: {x: number, y: number}
	 *                               				...
	 * 											}
	 */
	function layoutFunc(nodes, bb, options){
		if (!options) options = {};
		// console.log('running');
		newNodes = JSON.parse(JSON.stringify(nodes));
		// newLinks = JSON.parse(JSON.stringify(links));

		 var simulation = d3.forceSimulation(newNodes)
					      .force("charge", d3.forceManyBody())
					      // .force("link", d3.forceLink(newLinks).distance(20).strength(1))
					      .force("x", d3.forceX())
					      .force("y", d3.forceY())
								// .force('center', d3.forceCenter(options.center.x, options.center.y))
								// .force('r', d3.forceRadial(options.r))
								.alphaDecay(0.02)
					      .stop();
		var n = Math.ceil(Math.log(simulation.alphaMin()) / Math.log(1 - simulation.alphaDecay()))
			// console.log(n);
		for (var i = 0; i < n; ++i) {
		    // postMessage({type: "ticks", progress: i / n});
		    simulation.tick();
		}

		// postMessage({type: "end", nodes: newNodes, links: newLinks});

		var pos = {};

		for (var i = 0; i < newNodes.length; i++) {
			pos[newNodes[i].id] = {
				x: newNodes[i].x,
				y: newNodes[i].y,
			}
		}

		if (options.promise) {
			var promise = options.promise;
			if (Object.keys(pos).length > 0) {
				return promise.resolve(pos);
			} else {
				return promise.reject('Pos Empty');
			}
		} else {
			return pos;
		}

	}

	return layoutFunc
}())


onmessage = function(e){
	console.log('get message');
	layout(e.data.graph.nodes, e.data.graph.links, null, e.data.options);
}
